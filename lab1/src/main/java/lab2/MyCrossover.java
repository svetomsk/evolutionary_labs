package lab2;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static lab2.MyAlg.prepareDouble;

public class MyCrossover extends AbstractCrossover<List<Double>> {
    protected MyCrossover() {
        super(1);
    }

    protected List<List<Double>> mate(List<Double> p1, List<Double> p2, int numberOfCrossoverPoints, Random random) {
        return ordinaryCrossover(p1, p2, numberOfCrossoverPoints, random);
    }

    private List<List<Double>> ordinaryCrossover(List<Double> p1, List<Double> p2, int numberOfCrossoverPoints, Random random) {
        List<List<Double>> children = new ArrayList<>();
        // your implementation:
        List<Integer> splitPoints = random.ints(numberOfCrossoverPoints, 0, p1.size())
                .distinct()
                .limit(numberOfCrossoverPoints)
                .boxed()
                .collect(Collectors.toList());
        List<Double> c1 = new ArrayList<>();
        List<Double> c2 = new ArrayList<>();
        splitPoints.add(p1.size());
        int currentPoint = 0;
        boolean even = random.nextInt() % 2 == 0;
        for(Integer point : splitPoints) {
            int nextPoint = point;
            for (int i = currentPoint; i < nextPoint; i++) {
                if(even) {
                    c1.add(p1.get(i));
                    c2.add(p2.get(i));
                } else {
                    c1.add(p2.get(i));
                    c2.add(p1.get(i));
                }
            }
            even = !even;
            currentPoint = nextPoint;
        }

        children.add(c1);
        children.add(c2);
        return children;
    }
}
