package lab2;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.List;
import java.util.Random;


public class MyMutation implements EvolutionaryOperator<List<Double>> {
    private int counter = 0;
    private int generationsCount;

    public MyMutation(int generations) {
        this.generationsCount = generations;
    }

    public List<List<Double>> apply(List<List<Double>> population, Random random) {
        counter++;
        double mutationStrength = 0.15 * (1.0 - (double) counter / (double) generationsCount) + 0.01;
        double mutationRate = 0.35;
        double sigma = 0.5 * (1.0 - (double) counter / generationsCount) + 0.01;
        for (List<Double> individual : population) {
            if (Math.random() < mutationRate) {
                for (int i = 0; i < individual.size(); i++) {
                    if (Math.random() < mutationStrength) {
                        individual.set(i, individual.get(i) + random.nextGaussian() * sigma);
                    }
                }
            }
        }

        return population;
    }
}