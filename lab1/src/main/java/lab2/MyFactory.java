package lab2;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyFactory extends AbstractCandidateFactory<List<Double>> {

    private int dimension;

    public MyFactory(int dimension) {
        this.dimension = dimension;
    }

    public List<Double> generateRandomCandidate(Random random) {
        List<Double> solution = new ArrayList<>();
        // x from -5.0 to 5.0

        // your implementation:
        for(int i = 0; i < dimension; i++) {
            solution.add(random.nextDouble() % 6 * Math.pow(-1, random.nextInt() % 2));
        }

        return solution;
    }
}

