package lab2;

//import org.knowm.xchart.QuickChart;
//import org.knowm.xchart.SwingWrapper;
//import org.knowm.xchart.XYChart;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RankSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;
import org.uncommons.watchmaker.framework.termination.Stagnation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyAlg {
    public static double prepareDouble(double value, double maxValue, double minValue) {
        if (value > maxValue) {
            value = Math.signum(value) * maxValue;
        }
        if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    public static void main(String[] args) {
        List<Double> results = new ArrayList<>();
        double sum = 0;
        for(int i = 0; i < 1; i++) {
            double cur = run();
            results.add(cur);
            sum += cur;
            System.out.println(cur);
        }
        System.out.println(sum / (double) results.size());
    }

    public static double run() {
        int dimension = 100; // dimension of problem
        int populationSize =100; // size of population
        int generations = 10000; // number of generations

        Random random = new Random(); // random

        CandidateFactory<List<Double>> factory = new MyFactory(dimension); // generation of solutions

        ArrayList<EvolutionaryOperator<List<Double>>> operators = new ArrayList<>();
        operators.add(new MyCrossover()); // Crossover
        operators.add(new MyMutation(generations)); // Mutation
        EvolutionPipeline<List<Double>> pipeline = new EvolutionPipeline<List<Double>>(operators);

        SelectionStrategy<Object> selection = new RankSelection(); // Selection operator

        FitnessEvaluator<List<Double>> evaluator = new FitnessFunction(dimension); // Fitness function

        SteadyStateEvolutionEngine<List<Double>> algorithm = new SteadyStateEvolutionEngine<List<Double>>(
                factory, pipeline, evaluator, selection, populationSize, false, random);

        List<Double> population_best = new ArrayList<>();
        List<Double> population_mean = new ArrayList<>();
        List<List<Double>> best_values = new ArrayList<>();
        final double [] bestFits = new double[1];
        algorithm.addEvolutionObserver(populationData -> {
            double bestFit = populationData.getBestCandidateFitness();
            bestFits[0] = bestFit;
//            System.out.println("Generation " + populationData.getGenerationNumber() + ": " + bestFit);
            population_best.add(populationData.getBestCandidateFitness());
            population_mean.add(populationData.getMeanFitness());

            best_values.add(populationData.getBestCandidate());
        });
        TerminationCondition terminate = new GenerationCount(generations);
        TerminationCondition stady = new Stagnation(1000, true);
        algorithm.setSingleThreaded(true);
        algorithm.evolve(populationSize, 1, terminate, stady);
        List<Double> x = new ArrayList<>();
        for (int i = 0; i < population_best.size(); i++) {
            x.add((double) i);
        }

        System.out.println(x.size() + " " + population_best.size());

        return bestFits[0];
//        XYChart chart = QuickChart.getChart("title", "x", "y", "y(x)", toArray(x),
//                toArray(population_best));
//        chart.addSeries("yy(x)", toArray(x), toArray(population_mean));
//        new SwingWrapper(chart).displayChart();
    }

    public static double[] toArray(List<Double> val) {
        double[] value = new double[val.size()];
        for (int i = 0; i < value.length; i++) {
            value[i] = val.get(i);
        }
        return value;
    }
}

// dimension 10
// 10: