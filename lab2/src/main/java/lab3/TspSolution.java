package lab3;

import java.util.ArrayList;
import java.util.List;

public class TspSolution {
    // any required fields and methods
    private List<Integer> permutation;
    private int dimension;

    public TspSolution(List<Integer> permutation) {
        this.permutation = permutation;
        this.dimension = permutation.size();
    }

    public TspSolution(int dimension) {
        permutation = new ArrayList<Integer>();
        for (int i = 0; i < dimension; i++) {
            permutation.add(0);
        }
        this.dimension = dimension;
    }

    public void setValue(int pos, int value) {
        permutation.set(pos, value);
    }

    public int getValue(int pos) {
        return permutation.get(pos);
    }

    public List<Integer> getPermutation() {
        return permutation;
    }

    @Override
    public String toString() {
        String result = "";
        for(Integer s : permutation) {
            result += s + " ";
        }
        return result;
    }

    public int getDimension() {
        return dimension;
    }
}
