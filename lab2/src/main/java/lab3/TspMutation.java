package lab3;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class TspMutation implements EvolutionaryOperator<TspSolution> {
    private double count = 0;
    private double generations = 0;
    public TspMutation(int generations) {
        this.generations = generations;
    }

    public List<TspSolution> apply(List<TspSolution> population, Random random) {
        count++;
        // your implementation:
        double mutationRate = 0.35;
        for(TspSolution solution : population) {
            if(Math.random() < mutationRate) {
                exchangeMutation(solution, random);
            }
        }
        return population;
    }

    private void exchangeMutation(TspSolution solution, Random random) {
        Point ab = selectWithReduction(solution, random);
        int a = ab.x;
        int b = ab.y;
        int tmp = solution.getValue(a);
        solution.setValue(a, solution.getValue(b));
        solution.setValue(b, tmp);
    }

    private void insertionMutation(TspSolution solution, Random random) {
        Point ab = selectWithReduction(solution, random);
        int a = ab.x;
        int b = ab.y;
        List<Integer> perm = solution.getPermutation();
        perm.add(a + 1, perm.get(b));
        perm.remove(b + 1);
    }
    
    private void scrambleMutation(TspSolution solution, Random random) {
        Point ab = selectWithReduction(solution, random);
        int a = ab.x;
        int b = ab.y;
        List<Integer> sublist = solution.getPermutation().subList(a, b);
        Collections.shuffle(sublist);
        for (int i = a; i < b; i++) {
            solution.setValue(i, sublist.get(i - a));
        }
    }

    private void inversionMutation(TspSolution solution, Random random) {
        Point ab = selectWithReduction(solution, random);
        int a = ab.x;
        int b = ab.y;
        List<Integer> sublist = solution.getPermutation().subList(a, b);
        Collections.reverse(sublist);
        for (int i = a; i < b; i++) {
            solution.setValue(i, sublist.get(i - a));
        }
    }

    private void inversionMutationReduced(TspSolution solution, Random random) {
        Point ab = selectWithReduction(solution, random);
        int a = ab.x;
        int b = ab.y;

        List<Integer> sublist = solution.getPermutation().subList(a, b);
        Collections.reverse(sublist);
        for (int i = a; i < b; i++) {
            solution.setValue(i, sublist.get(i - a));
        }
    }

    private Point selectRandom(TspSolution solution, Random random) {
        int n = solution.getDimension();
        int a = random.nextInt(n);
        int b = random.nextInt(n);
        while(b == a) {
            b = random.nextInt(n);
        }
        int tmp = a;
        a = Math.min(a, b);
        b = Math.max(tmp, b);
        return new Point(a, b);
    }

    private Point selectWithReduction(TspSolution solution, Random random) {
        int n = solution.getDimension();
        int maxLength = (int)(n * (1.0 - count / generations));
        int currentLength = Math.max(1, random.nextInt(Math.max(2, maxLength + 1)));
        int a = random.nextInt(n - currentLength);
        int b = a + currentLength;

        return new Point(a, b);
    }
}
