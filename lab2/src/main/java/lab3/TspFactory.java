package lab3;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TspFactory extends AbstractCandidateFactory<TspSolution> {



    public TspSolution generateRandomCandidate(Random random) {
        int pointsCount = TspFitnessFunction.count;
        List<Integer> result = Stream.iterate(0, x -> x + 1).limit(pointsCount).collect(Collectors.toList());
        Collections.shuffle(result);
        return new TspSolution(result);
    }
}

