package lab3;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TspFitnessFunction implements FitnessEvaluator<TspSolution> {
    public static int count;
    private List<Point> points;


    public TspFitnessFunction(String problem) {
        points = new ArrayList<Point>();
        try {
            Scanner sc = new Scanner(new File(problem + ".txt"));
            count = sc.nextInt();
            for (int i = 0; i < count; i++) {
                int pos = sc.nextInt();
                int x = sc.nextInt();
                int y = sc.nextInt();
                points.add(new Point(x, y));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public double getFitness(TspSolution solution, List<? extends TspSolution> list) {
        List<Integer> permutation = solution.getPermutation();
        double result = 0;
        for (int i = 0; i < permutation.size() - 1; i++) {
            int first = permutation.get(i);
            int second = permutation.get(i + 1);
            result += distance(points.get(first), points.get(second));
        }
        result += distance(points.get(permutation.get(0)), points.get(permutation.get(permutation.size() - 1)));
        return result;
    }

    public List<double[]> points(TspSolution solution) {
        double [] x = new double[solution.getDimension()];
        double [] y = new double[solution.getDimension()];
        int cnt = 0;
        for(Integer i : solution.getPermutation()) {
            x[cnt] = points.get(i).x;
            y[cnt] = points.get(i).y;
            cnt++;
        }
        List<double[]> res = new ArrayList<>();
        res.add(x);
        res.add(y);
        return res;
    }

    private double distance(Point p1, Point p2) {
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    public boolean isNatural() {
        return false;
    }
}
