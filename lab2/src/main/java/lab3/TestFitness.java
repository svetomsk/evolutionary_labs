package lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestFitness {
    public static void main(String[] args) throws FileNotFoundException {
        List<Integer> path = new ArrayList<>();
        Scanner sc = new Scanner(new File("data.txt"));
        for (int i = 0; i < 343; i++) {
            path.add(sc.nextInt() - 1);
        }
        TspSolution sol = new TspSolution(path);
        System.out.println(new TspFitnessFunction("xqf343").getFitness(sol, null));
    }
}
