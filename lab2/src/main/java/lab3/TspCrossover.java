package lab3;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.*;

public class TspCrossover extends AbstractCrossover<TspSolution> {
    protected TspCrossover() {
        super(1);
    }

    protected List<TspSolution> mate(TspSolution p1, TspSolution p2, int i, Random random) {
        int n = p1.getDimension();
        List<TspSolution> children = new ArrayList<>();
        i = random.nextInt(n);
        int b = random.nextInt(n);
        while(b == i) {
            b = random.nextInt(n);
        }
        int tmp = i;
        i = Math.min(i, b);
        b = Math.max(tmp, b);
        List<Integer> first = p1.getPermutation();
        List<Integer> second = p2.getPermutation();
        children.add(new TspSolution(exchange(first, second, i, b)));
        children.add(new TspSolution(exchange(second, first, i, b)));
        return children;
    }

    private List<Integer> exchange(List<Integer> first, List<Integer> second, int a, int b) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < first.size(); i++) {
            result.add(-1);
        }
        Set<Integer> used = new HashSet<>();
        for (int i = a; i <= b; i++) {
            result.set(i, first.get(i));
            used.add(first.get(i));
        }
        LinkedList<Integer> absent = new LinkedList<>();
        for (int i = a; i < second.size(); i++) {
            if(!used.contains(second.get(i))) {
                absent.add(second.get(i));
            }
        }
        for (int i = 0; i < a; i++) {
            if(!used.contains(second.get(i))) {
                absent.add(second.get(i));
            }
        }
        for (int i = 0; i < result.size(); i++) {
            if(result.get(i) == -1) {
                result.set(i, absent.pollFirst());
            }
        }
        return result;
    }
}

class Test {
    public static void main(String[] args) {
        List<Integer> first = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            first.add(i + 1);
        }
        List<Integer> second = new ArrayList<>();
        second.add(3);
        second.add(7);
        second.add(5);
        second.add(2);
        second.add(8);
        second.add(1);
        second.add(4);
        second.add(9);
        second.add(6);

        TspCrossover crossover = new TspCrossover();
        List<TspSolution> list = new ArrayList<>();
        list.add(new TspSolution(first));
        list.add(new TspSolution(second));
        System.out.println(crossover.mate(list.get(0), list.get(1), 3, new Random()));
    }
}
