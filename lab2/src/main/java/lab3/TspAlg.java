package lab3;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RankSelection;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.selection.TournamentSelection;
import org.uncommons.watchmaker.framework.selection.TruncationSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TspAlg {

    public static void main(String[] args) {
        System.out.println(new Random().nextInt() % 6);
        String problem = "xqf131"; // name of problem or path to input file

        int populationSize = 100; // size of population
        int generations = 10000; // number of generations

        Random random = new Random(); // random

        CandidateFactory<TspSolution> factory = new TspFactory(); // generation of solutions

        ArrayList<EvolutionaryOperator<TspSolution>> operators = new ArrayList<EvolutionaryOperator<TspSolution>>();
        operators.add(new TspCrossover()); // Crossover
        operators.add(new TspMutation(generations)); // Mutation
        EvolutionPipeline<TspSolution> pipeline = new EvolutionPipeline<TspSolution>(operators);

        SelectionStrategy<Object> selection = new RankSelection(); // Selection operator

        TspFitnessFunction evaluator = new TspFitnessFunction(problem); // Fitness function
//        GenerationalEvolutionEngine<TspSolution> algorithm = new GenerationalEvolutionEngine<>(factory, pipeline, evaluator, selection, random);
        EvolutionEngine<TspSolution> algorithm = new EvolutionStrategyEngine<>(factory, pipeline, evaluator, true, 2, new Random());
//        EvolutionEngine<TspSolution> algorithm = new SteadyStateEvolutionEngine<TspSolution>(
//                factory, pipeline, evaluator, selection, populationSize, false, random);
        final XYChart[] chart = {null};
        final SwingWrapper[] wrapper = {null};
        algorithm.addEvolutionObserver(new EvolutionObserver() {
            public void populationUpdate(PopulationData populationData) {
                double bestFit = populationData.getBestCandidateFitness();
                System.out.println("Generation " + populationData.getGenerationNumber() + ": " + bestFit);
                TspSolution best = (TspSolution)populationData.getBestCandidate();
                System.out.println("\tBest solution = " + best.toString());
                List<double[]> xy = evaluator.points(best);
//                if(chart[0] == null) {
                System.out.println(wrapper.length);
                if(chart[0] == null) {
                    chart[0] = QuickChart.getChart("Title", "x", "y", "1", xy.get(0), xy.get(1));
                    wrapper[0] = new SwingWrapper(chart[0]);
                    wrapper[0].displayChart();
                } else {
                    chart[0].updateXYSeries("1", xy.get(0), xy.get(1), null);
                    wrapper[0].repaintChart();
                }

            }
        });

        TerminationCondition terminate = new GenerationCount(generations);
        algorithm.evolve(populationSize, 1, terminate);
    }
}
